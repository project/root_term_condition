<?php

namespace Drupal\root_term_condition\Plugin\Condition;

use Drupal\Core\Condition\ConditionPluginBase;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Routing\CurrentRouteMatch;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\taxonomy\TermInterface;
use Drupal\taxonomy\TermStorageInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Condition that is true when the term has no parent.
 *
 * @Condition(
 *   id = "root_term_condition",
 *   label = @Translation("Root term"),
 * )
 */
class RootTermCondition extends ConditionPluginBase implements ContainerFactoryPluginInterface {

  /**
   * The current route match.
   *
   * @var \Drupal\Core\Routing\CurrentRouteMatch
   */
  private CurrentRouteMatch $routeMatch;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  private EntityTypeManagerInterface $entityTypeManager;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition): RootTermCondition|static {
    $instance = new static($configuration, $plugin_id, $plugin_definition);

    $instance->routeMatch = $container->get('current_route_match');
    $instance->entityTypeManager = $container->get('entity_type.manager');

    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration(): array {
    $config = parent::defaultConfiguration();
    $config['enable'] = FALSE;

    return $config;
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state): array {
    $form = parent::buildConfigurationForm($form, $form_state);

    $form['negate']['#description'] = $this->t('This will display the block only terms with parents.');
    $form['negate']['#weight'] = 2;

    $form['enable'] = [
      '#title' => $this->t('Display only on root terms'),
      '#type' => 'checkbox',
      '#default_value' => $this->configuration['enable'],
      '#description' => $this->t('When this box is checked, this block will only be shown on terms with no parents.'),
      '#weight' => 1,
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state): void {
    if ($form_state->getValue('enable')) {
      $this->configuration['enable'] = TRUE;
    }
    else {
      $this->configuration = [];
    }

    parent::submitConfigurationForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function evaluate(): bool {
    if (!$this->configuration['enable']) {
      return FALSE;
    }

    $storage = $this->entityTypeManager->getStorage('taxonomy_term');
    $term = $this->routeMatch->getParameter('taxonomy_term');
    if ($term instanceof TermInterface && $storage instanceof TermStorageInterface) {
      return empty($storage->loadParents($term->id()));
    }

    // Not on a term.
    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function summary(): TranslatableMarkup {
    if ($this->configuration['enable']) {
      return $this->t('Enabled');
    }

    return $this->t('Not restricted');
  }

}
